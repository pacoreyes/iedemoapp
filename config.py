import os.path

# Initialize root URL of the project
root_url = "https://www.ted.com"

# Initialize base dir of the project
base_dir = os.path.dirname(os.path.abspath(__file__))
