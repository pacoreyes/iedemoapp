# IEDApp

IEDApp is a Python App for scraping the TED Talks website. It was created for educational purposes 
for the seminar "Information Extraction from Web Resources", given by Juan-Francisco Reyes (reyes@b-tu.edu) at the 
Department of Internet Technology in Brandenburg University of Technology, Cottbus, Germany.

## Installation

Refer to the website http://www.informationsextraktion.com.

## License
[MIT](https://choosealicense.com/licenses/mit/)