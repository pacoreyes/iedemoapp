""" Tweets Controller layer """
import app.m.twitter as twitterModel
import app.m.tweet as tweetModel
import app.v.tweets as tweetView


# Extract Tweets from Twitter API
def extract():
    # Retrieve all Twitter accounts from database
    twitters = twitterModel.retrieve_all()
    for t in twitters:
        # Extract Tweets by user name
        tweets = tweetModel.extract(t.screen_name)
        for tw in tweets:
            # Invoke add() procedure that stores Tweets in the database
            response = tweetModel.add(t, tw)
            # Invoke view
            tweetView.extract(response, tw['id'])
