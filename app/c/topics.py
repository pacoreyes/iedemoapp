""" Topic Controller layer """
import app.m.talk as talkModel
import app.m.twitter as twitterModel
import app.m.tweet as tweetModel
import app.m.topic as topicModel
import app.v.topics as topicView
import helpers


# Extract Topic from Text composed by Tweets, Speaker Bio and Talk Description
def extract():
    # Retrieve all Twitter accounts from database
    twitters = twitterModel.retrieve_all()
    for t in twitters:
        # Prepare Text to be analysed by Google NLP (1, 2, 3, 4, 5)
        # 1. Extract Tweets from Twitter API
        tweets = tweetModel.retrieve(t.screen_name)
        # 2. Basic Tweets data sanitization
        tweets_text = helpers.sanitize_tweet_data(tweets)
        # 3. Extract Speaker bio
        speaker = twitterModel.get_speaker_from_twitter(t.screen_name)
        # 4. Extract Talk description
        talks_description = talkModel.get_talk_from_speaker(speaker)
        # 5. Put all string together
        text = speaker['bio'] + talks_description + tweets_text
        # Extract Topics from Text (Tweets, Speaker Bio and Talk Description)
        topics = topicModel.extract(text)
        if not topics:
            continue
        for topic in topics:
            # Add topic record linked to an Speaker
            response = topicModel.add(topic, t)
            # View of extract procedure
            topicView.extract(response, topic,
                              speaker['firstname'] + " " + speaker['lastname'])
