""" Speaker Controller layer """
import app.m.talk as talkModel
import app.m.speaker as speakerModel
import app.v.speakers as speakersView
import helpers


# Control Speakers scraping
def scrape_all():
    # Retrieve all Talk records from the database
    talks = talkModel.retrieve_all()
    # Scrape Talks listing from web page using the URL
    for t in talks:
        # Scrape and parse each Talk web page to get Speaker data
        talk = talkModel.scrape(t.url)
        speakers = speakerModel.parse(talk)
        # Talks might have more than one speaker, iteration is needed
        for s in speakers:
            # Delay randomly scraping time
            helpers.set_random_delay(1, 10)
            # Save Speaker record in the database
            response = speakerModel.add(s, t.title)
            speaker = speakerModel.retrieve(s)
            # Invoke view
            speakersView.scrape_all(response, speaker.get_full_name())
