""" Talk Controller layer """
import app.m.category as categoryModel
import app.m.talk as talkModel
import app.v.talks as talksView
import helpers


# Control scraping of all Talks belonging to a Category
def scrape_all(category):
    # Retrieve Category object
    category = categoryModel.retrieve(category)
    # Get number of pages in Category page
    pages = categoryModel.get_category_pages_number(category)
    for page in pages:
        # Define URL based on which page is located
        url = talkModel.talk_url_by_page_number(category, page)
        # Scrape index of Talks from page
        talks = talkModel.scrape_all(url)
        for t in talks:
            # Scrape individually each Talk
            data = talkModel.scrape(t)
            talk = talkModel.parse(data)
            # Delay randomly scraping time
            helpers.set_random_delay(1, 10)
            # Invoke add() procedure that stores Category in the database
            response = talkModel.add(talk)
            # Invoke view
            talksView.scrape(response, talk['title'])


# Control scraping 1 Talk
def scrape(url):
    # Scrape individual Talk
    data = talkModel.scrape(url)
    talk = talkModel.parse(data)
    # Invoke add() procedure that stores Talk in the database
    response = talkModel.add(talk)
    # Invoke view
    talksView.scrape(response, talk['title'])
