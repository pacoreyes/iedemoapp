""" Category Controller layer """
import app.m.category as categoryModel
import app.v.categories as categoriesView


# Control Categories scraping
def scrape_all():
    # Scrape Categories from web page by HTTP request
    categories = categoryModel.scrape_all()
    for c in categories:
        # Invoke add category procedure that stores data in the database
        response = categoryModel.add(c)
        # Invoke view
        categoriesView.scrape_all(response, c)


# Verify if Category URLs are alive by HTTP request
def verify_urls():
    # Retrieve all Category records from the database
    categories = categoryModel.retrieve_all()
    for c in categories:
        # Verify HTTP request status
        status = categoryModel.verify_urls(c)
        # Invoke view
        categoriesView.verify_urls(status, c)
