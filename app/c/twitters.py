""" Twitter Controller layer """
import app.m.speaker as speakerModel
import app.m.talk as talkModel
import app.m.twitter as twitterModel
import app.v.twitters as twittersView


# Link all Speakers to Twitter accounts
def link_all():
    # Retrieve all Speaker records from the database
    speakers = speakerModel.retrieve_all()
    for s in speakers:
        # Search on Twitter accounts by Speaker full name
        twitter_accounts = twitterModel.search_user_in_twitter(s.get_full_name())
        while True:
            # Get number of result items (Twitter accounts)
            num_result_items = len(twitter_accounts)
            if num_result_items > 0:
                # Retrieve all Talks related to Speaker
                talks = talkModel.retrieve(s)
                # Show menu options and store 'chosen' Twitter account
                choice = twittersView.link_all_menu(s, talks, twitter_accounts)
                # Get the position in result of chosen Twitter account
                item_number = twitter_accounts[choice - 1]
                # Link Speaker to chosen Twitter account
                if 1 <= choice <= num_result_items:
                    # Invoke add() procedure that stores Twitter account
                    twitterModel.add(item_number, s)
                    twittersView.add(item_number, s)
                    break
                elif choice == 0:  # Skip Speaker
                    twittersView.skip()
                    break
                elif choice == 99:  # Unlink Speaker
                    twitterModel.delete(s)
                    break
                else:
                    # Do nothing if choice is not within the inspected choices
                    twittersView.wrong()
            else:
                # View for not found Twitter account for Speaker
                twittersView.not_found(s)
                break
