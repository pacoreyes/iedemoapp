""" Main Controller procedures and parameters """
from neo4j import GraphDatabase
from requests_html import HTMLSession
from requests_oauthlib import OAuth1
import helpers
import json
import os

# Initialize request-HTML session
session = HTMLSession()


# HTTP request a URL
def get_by_http_request(url):
    # Initialize Request-HTML module session for HTML parsing
    return session.get(url)


# Set Twitter API credential driver
def twitter_api_driver():
    twitter_credentials_path = "credentials/twitter-api-credentials.json"
    with open(twitter_credentials_path) as file:
        twitter_credentials = json.load(file)
    return OAuth1(
        twitter_credentials['app-key'],
        twitter_credentials['app-secret'],
        twitter_credentials['oauth-token'],
        twitter_credentials['oauth-token-secret']
    )


# Initialize Twitter API driver
twitter_api = twitter_api_driver()


# Set Neo4J database driver
def neo4j_database_driver():
    ne4j_credentials = \
        helpers.open_file("credentials/",
                          "neo4j-credentials",
                          "json")
    neo4j_auth = (ne4j_credentials["user"],
                  ne4j_credentials["password"])
    return GraphDatabase.driver(ne4j_credentials["uri"],
                                auth=neo4j_auth)


# Initialize Neo4j database driver
db = neo4j_database_driver()


# Read Google GCP credential driver
os.environ['GOOGLE_APPLICATION_CREDENTIALS'] \
    = 'credentials/google-cloud-credentials.json'
