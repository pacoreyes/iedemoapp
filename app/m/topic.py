""" Topic Model layer """
from app.c.main import db
from google.cloud import language_v1


# Topic Class constructor
class Topic:
    """Topic Class"""
    def __init__(self, name):
        self.name = name


# Save Topic record in the database
def add(slots, twitter):
    try:
        # Open database session
        with db.session() as session:
            # Initiate transaction
            tx = session.begin_transaction()
            # Retrieve Speaker, owner of Twitter account
            speaker = tx.run(
                "MATCH (t:Twitter)<-[:HAS_TWITTER]-(speaker) "
                "WHERE t.screen_name = $screen_name "
                "RETURN speaker",
                screen_name=twitter.screen_name
            )
            speaker = speaker.data()[0]['speaker']
            # Check if a the Topic exists, and if not it is created
            # along with a relationship between Speaker and Topic
            tx.run(
                "MERGE (t:Topic {name: $name}) "
                "MERGE (s:Speaker {firstname: $firstname, "
                "middleinitial: $middleinitial, "
                "lastname: $lastname}) "
                "MERGE (s)-[r:SPEAKS_ABOUT {confidence: $confidence}]->(t)",
                firstname=speaker['firstname'],
                middleinitial=speaker['middleinitial'],
                lastname=speaker['lastname'],
                name=slots['name'],
                confidence=slots['confidence']
            )
            # Commit and close transaction
            tx.commit()
            tx.close()
            # Close database session
            db.close()
            return True
    except Exception as error:
        return error


# Returns all Topic
def retrieve_all():
    try:
        # Initialize list of resulting Topic
        result = []
        # Open Neo4j session and driver
        session = db.session()
        # Retrieve all Tweet records
        topics = session.run(
            "MATCH (t:Topic) RETURN t AS topic"
        )
        for t in topics:
            topic = t.data()['topic']
            # Instantiate Class object
            topic = Topic(
                topic['name']
            )
            result.append(topic)
        # Close Neo4j session and driver
        session.close()
        return result
    except Exception as error:
        return error


# Extract categories from Speaker content using Google NL API
def extract(text):
    client = language_v1.LanguageServiceClient()
    # Available types: PLAIN_TEXT, HTML
    type_ = language_v1.Document.Type.PLAIN_TEXT
    language = "en"
    document = {"content": text, "type_": type_, "language": language}
    response = client.classify_text(request={'document': document})
    # Loop through categories returned from the Google NL API
    return [{'name': c.name[1:], 'confidence': c.confidence} for c in response.categories]
