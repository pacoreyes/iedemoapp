""" Talk Model layer """
from app.c.main import db, get_by_http_request
from config import root_url
import helpers
import re


# Talk Class constructor
class Talk:
    """Talk Class"""
    def __init__(self, title, description, year, url, image_url):
        self.title = title
        self.description = description
        self.year = year
        self.url = url
        self.image_url = image_url


# Save Talk record in the database
def add(slots):
    try:
        # Open database session
        with db.session() as session:
            # Initiate transaction
            tx = session.begin_transaction()
            # Evaluate if Talk exists
            exists = tx.run(
                "MATCH (t:Talk {title: $title}) RETURN t",
                title=slots['title']
            )
            if exists.single() is None:
                # Get list of categories
                categories = slots['categories']
                # Drop categories property from slot
                del slots['categories']
                # Save record in database
                tx.run(
                    "CREATE (t:Talk $slots)",
                    slots=slots
                )
                # Clean title removing symbols and spaces
                title = re.sub(r'[^\w] ', ' ', slots['title'])
                # Replace spaces with underline
                title = re.sub(' +', '_', title).lower()
                helpers.save_file(
                    slots,
                    "data/talks_json/",
                    "ted_talk_" + title,
                    "json"
                )
                # Create relationships between Talk and Category
                for c in categories:
                    tx.run(
                        "MATCH (t:Talk),(c:Category) "
                        "WHERE t.title = $title "
                        "AND toLower(c.name) = $category_name "
                        "CREATE (t)-[r:IN_CATEGORY]->(c)",
                        title=slots['title'],
                        category_name=c
                    )
                # Commit and close transaction
                tx.commit()
                tx.close()
                # Close database session
                db.close()
                return True
            else:
                return False
    except Exception as error:
        return error


# Returns all Talks as list of Talks objects
def retrieve_all():
    try:
        # Initialize list of resulting talks
        result = []
        # Open Neo4j session and driver
        session = db.session()
        # Retrieve all Talk records
        talks = session.run(
            "MATCH (t:Talk) RETURN t AS talk"
        )
        for t in talks:
            talk = t.data()['talk']
            # Instantiate Class object
            talk = Talk(
                talk['title'],
                talk['description'],
                talk['year'],
                talk['url'],
                talk['image_url']
            )
            result.append(talk)
        # Close Neo4j session and driver
        session.close()
        return result
    except Exception as error:
        return error


# Returns all Talk records by speaker
def retrieve(speaker):
    try:
        # Initialize list of resulting talks
        result = []
        # Open Neo4j session and driver
        session = db.session()
        # Retrieve all Talk records by speaker
        talks = session.run(
            "MATCH (s:Speaker)-[:SPOKE_AT]->(talk) "
            "WHERE s.firstname = $firstname "
            "AND s.middleinitial = $middleinitial "
            "AND s.lastname = $lastname "
            "RETURN talk",
            firstname=speaker.firstname,
            middleinitial=speaker.middleinitial,
            lastname=speaker.lastname
        )
        for t in talks:
            talk = t.data()['talk']
            # Instantiate Class object
            talk = Talk(
                talk['title'],
                talk['description'],
                talk['year'],
                talk['url'],
                talk['image_url']
            )
            result.append(talk)
        # Close Neo4j session and driver
        session.close()
        return result
    except Exception as error:
        return error


# Scrape HTML elements from web page
def scrape_all(url):
    data = get_by_http_request(url)
    # Store Category elements in a list
    talks = data.html.find('div.row-skinny', first=False)
    talks = talks[7].find('div.col')
    talk_urls = []
    for t in talks:
        # Collect urls
        talk_url = root_url + t.find('a', first=True).attrs['href']
        talk_urls.append(talk_url)
    return talk_urls


# Scrape JSON object from embedded in HTML elements from web page
def scrape(url):
    # HTTP request the Talk page
    data = get_by_http_request(url)
    # Parse talk elements in a string
    data = data.html.find('script', first=False)[11].text
    # Clean data and get it back as JSON object
    data = helpers.sanitize_talk_data(data)
    return data


# Parse data from scraped JSON object
def parse(data):
    slots = {'title': data['talks'][0]['player_talks'][0]['title'],
             'description': data['description'],
             'year': int(data['talks'][0]['player_talks'][0]
                         ['resources']['hls']['maiTargeting']['year']),
             'url': data['url'],
             'image_url': data['talks'][0]['hero'],
             'categories': data['talks'][0]['tags']}
    return slots


# Create Talk's URL for pagination
def talk_url_by_page_number(category, page):
    if page == 1:
        url = root_url + "/talks?topics%5B%5D=" + category.get_slug()
    else:
        url = root_url + "/talks?page=" + str(page) \
              + "&topics%5B%5D=" + category.get_slug()
    return url


# Get Talk records by Speaker
def get_talk_from_speaker(speaker):
    session = db.session()
    # Retrieve all Tweet records
    talks = session.run(
        "MATCH (s:Speaker)-[:SPOKE_AT]->(talks) "
        "WHERE s.firstname = $firstname "
        "AND s.middleinitial = $middleinitial "
        "AND s.lastname = $lastname "
        "RETURN talks",
        firstname=speaker['firstname'],
        middleinitial=speaker['middleinitial'],
        lastname=speaker['lastname']
    )
    talks = [t['talks']['description'] for t in talks.data()]
    return talks[0]
