""" Tweet Model layer """
from app.c.main import db, twitter_api
import requests


# Tweet Class constructor
class Tweet:
    """Tweet Class"""

    def __init__(self, tweet_id, created_at,
                 text, hashtags, lang):
        self.id = tweet_id
        self.created_at = created_at
        self.text = text
        self.hashtags = hashtags
        self.lang = lang


# Save Tweet record in the database
def add(twitter, tweet):
    # Separate hashtags in a list
    hashtags = tweet['entities']['hashtags']
    hashtags = [h['text'] for h in hashtags]
    # Create slot of data
    slots = {'id': tweet['id'],
             'created_at': tweet['created_at'],
             'text': tweet['full_text'],
             'hashtags': hashtags,
             'lang': tweet['lang']}
    try:
        # Open database session
        with db.session() as session:
            # Initiate transaction
            tx = session.begin_transaction()
            # Evaluate if Tweet exists
            exists = tx.run(
                "MATCH (t:Tweet {id: $id}) RETURN t",
                id=slots['id']
            )
            if exists.single() is None:
                # Save record in database
                tx.run(
                    "CREATE (t:Tweet $slots)",
                    slots=slots
                )
                # #### Remove to store Tweet in JSON format #########
                # helpers.save_file(
                #    slots,
                #    "data/twitter_json/",
                #    "tweet_" + twitter.screen_name + slots['id'],
                #    "json"
                # )
                # Create relationship between Tweet and Twitter account
                tx.run(
                    "MATCH (t:Twitter),(n:Tweet) "
                    "WHERE t.screen_name = $screen_name "
                    "AND n.id = $id "
                    "CREATE (t)-[r:TWEETED]->(n)",
                    screen_name=twitter.screen_name,
                    id=slots['id']
                )
                # Commit and close transaction
                tx.commit()
                tx.close()
                # Close database session
                db.close()
                return True
            else:
                return False
    except Exception as error:
        return error


# Returns all Tweets
def retrieve_all():
    try:
        # Initialize list of resulting Tweets
        result = []
        # Open Neo4j session and driver
        session = db.session()
        # Retrieve all Tweet records
        tweets = session.run(
            "MATCH (t:Tweet) RETURN t AS tweet"
        )
        for t in tweets:
            tweet = t.data()['tweet']
            # Instantiate Class object
            tweet = Tweet(
                tweet['id'],
                tweet['created_at'],
                tweet['text'],
                tweet['hashtags'],
                tweet['lang']
            )
            result.append(tweet)
        # Close Neo4j session and driver
        session.close()
        return result
    except Exception as error:
        return error


# Returns all Tweets by Twitter account
def retrieve(twitter):
    try:
        # Initialize list of resulting talks
        result = []
        # Open Neo4j session and driver
        session = db.session()
        # Retrieve all Talk records by speaker
        tweets = session.run(
            "MATCH (t:Twitter)-[:TWEETED]->(tweet) "
            "WHERE t.screen_name = $screen_name "
            "RETURN tweet",
            screen_name=twitter
        )
        for t in tweets:
            tweet = t.data()['tweet']
            # Instantiate Class object
            tweet = Tweet(
                tweet['id'],
                tweet['created_at'],
                tweet['text'],
                tweet['hashtags'],
                tweet['lang']
            )
            result.append(tweet)
        # Close Neo4j session and driver
        session.close()
        return result
    except Exception as error:
        return error


# Extract Tweets from Twitter API by Twitter account
def extract(screen_name):
    # Extraction parameters
    count = 200  # number of Tweets returned
    exclude_replies = True  # Exclude replies
    include_rts = False  # Include retweets
    # Load Twitter credential file
    timeline_api_url = "https://api.twitter.com/1.1/statuses/user_timeline.json" \
                       "?screen_name={}&count={}&exclude_replies={}&include_rts={}&tweet_mode=extended" \
        .format(screen_name, count, str(exclude_replies).lower(), str(include_rts).lower())
    response = requests.get(timeline_api_url, auth=twitter_api)
    return response.json()
