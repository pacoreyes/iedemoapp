""" Speaker Model layer """
from app.c.main import db
import helpers
import re


# Speaker Class constructor
class Speaker:
    """Speaker Class"""

    def __init__(self, firstname, middleinitial, lastname,
                 description, bio, photo_url):
        self.firstname = firstname
        self.middleinitial = middleinitial
        self.lastname = lastname
        self.description = description
        self.bio = bio
        self.photo_url = photo_url

    # Method to build the whole name
    def get_full_name(self):
        return self.firstname + " " \
               + (self.middleinitial if self.middleinitial + " " else "") \
               + self.lastname


# Save Speaker data in the database
def add(slots, talk_title):
    try:
        # Open database session
        with db.session() as session:
            # Initiate transaction
            tx = session.begin_transaction()
            # Verify if Speaker record exists
            exists = tx.run(
                "MATCH (s:Speaker) "
                "WHERE s.firstname = $firstname "
                "AND s.middleinitial = $middleinitial "
                "AND s.lastname = $lastname "
                "RETURN s",
                firstname=slots['firstname'],
                middleinitial=slots['middleinitial'],
                lastname=slots['lastname']
            )
            if exists.single() is None:
                # Save record in database
                tx.run(
                    "CREATE (s:Speaker $slots)",
                    slots=slots
                )
                # Manipulate and clean names removing symbols and spaces
                names = [slots['firstname'],
                         slots['middleinitial'],
                         slots['lastname']]
                name = " "
                name = name.join(names)
                name = re.sub(r'[^\w] ', ' ', name)
                name = re.sub(' +', '_', name).lower()
                # Save data JSON file
                helpers.save_file(
                    slots,
                    "data/speakers_json/",
                    "ted_speaker_" + name,
                    "json"
                )
                # Create relationship between Speaker and Talk
                tx.run(
                    "MATCH (s:Speaker),(t:Talk) "
                    "WHERE s.firstname = $firstname "
                    "AND s.middleinitial = $middleinitial "
                    "AND s.lastname = $lastname "
                    "AND t.title = $title "
                    "CREATE (s)-[r:SPOKE_AT]->(t)",
                    firstname=slots['firstname'],
                    middleinitial=slots['middleinitial'],
                    lastname=slots['lastname'],
                    title=talk_title
                )
                # Commit and close transaction
                tx.commit()
                tx.close()
                # Close database session
                db.close()
                return True
            else:
                return False
    except Exception as error:
        return error


# Returns all speakers as list of Speaker objects
def retrieve_all():
    try:
        result = []
        # Open Neo4j session and driver
        session = db.session()
        # Retrieve all Speaker records
        speakers = session.run(
            "MATCH (s:Speaker) RETURN s AS speaker"
        )
        for s in speakers:
            speaker = s.data()['speaker']
            # Instantiate Class object
            speaker = Speaker(
                speaker['firstname'],
                speaker['middleinitial'],
                speaker['lastname'],
                speaker['description'],
                speaker['bio'],
                speaker['photo_url']
            )
            result.append(speaker)
        # Close Neo4j session and driver
        session.close()
        return result
    except Exception as error:
        return error


def retrieve(speaker):
    try:
        # Open Neo4j session and driver
        session = db.session()
        speaker = session.run(
            "MATCH (s:Speaker) "
            "WHERE s.firstname = $firstname "
            "AND s.middleinitial = $middleinitial "
            "AND s.lastname = $lastname "
            "RETURN s AS speaker",
            firstname=speaker['firstname'],
            middleinitial=speaker['middleinitial'],
            lastname=speaker['lastname']
        )
        speaker = speaker.data()[0]['speaker']
        speaker = Speaker(speaker['firstname'],
                          speaker['middleinitial'],
                          speaker['lastname'],
                          speaker['description'],
                          speaker['bio'],
                          speaker['photo_url'])
        # Close Neo4j session and driver
        session.close()
        # Instantiate Class object
        return speaker
    except Exception as error:
        return error


# Parse data from scraped JSON object
def parse(data):
    result = []
    speakers = data['speakers']
    for s in speakers:
        slots = {'firstname': s['firstname'],
                 'middleinitial': s['middleinitial'],
                 'lastname': s['lastname'],
                 'description': s['description'],
                 'bio': s['whotheyare'],
                 'photo_url': s['photo_url']}
        result.append(slots)
    return result
