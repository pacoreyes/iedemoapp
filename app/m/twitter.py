""" Twitter Model layer """
from app.c.main import db, twitter_api
import requests
import helpers


# Twitter Class constructor
class Twitter:
    """Twitter Class"""

    def __init__(self, account_id, screen_name, name,
                 description, location, profile_image_url):
        self.id = account_id
        self.screen_name = screen_name
        self.name = name
        self.description = description
        self.location = location
        self.profile_image_url = profile_image_url


# Save Twitter account record in the database
def add(twitter, speaker):
    slots = {'id': twitter['id'],
             'screen_name': twitter['screen_name'],
             'name': twitter['name'],
             'description': twitter['description'],
             'location': twitter['location'],
             'profile_image_url': twitter['profile_image_url']}
    try:
        # Open database session
        with db.session() as session:
            # Initiate transaction
            tx = session.begin_transaction()
            # Evaluate if Twitter account exists
            exists = tx.run(
                "MATCH (t:Twitter {screen_name: $screen_name}) RETURN t",
                screen_name=slots['screen_name']
            )
            if exists.single() is None:
                # Save record in database
                tx.run(
                    "CREATE (t:Twitter $slots)",
                    slots=slots
                )
                helpers.save_file(
                    slots,
                    "data/twitter_json/",
                    "twitter_" + slots['screen_name'],
                    "json"
                )
                # Create relationships between Twitter account and Speaker
                tx.run(
                    "MATCH (t:Twitter),(s:Speaker) "
                    "WHERE s.firstname = $firstname "
                    "AND s.middleinitial = $middleinitial "
                    "AND s.lastname = $lastname "
                    "AND t.screen_name = $screen_name "
                    "CREATE (s)-[r:HAS_TWITTER]->(t)",
                    firstname=speaker.firstname,
                    middleinitial=speaker.middleinitial,
                    lastname=speaker.lastname,
                    screen_name=slots['screen_name']
                )
                # Commit and close transaction
                tx.commit()
                tx.close()
                # Close database session
                db.close()
                return True
            else:
                return False
    except Exception as error:
        return error


# Returns all Twitter accounts
def retrieve_all():
    try:
        # Initialize list of resulting talks
        result = []
        # Open Neo4j session and driver
        session = db.session()
        # Retrieve all Talk records
        twitter = session.run(
            "MATCH (t:Twitter) RETURN t AS twitter"
        )
        for t in twitter:
            twitter = t.data()['twitter']
            # Instantiate Class object
            twitter = Twitter(
                twitter['id'],
                twitter['screen_name'],
                twitter['name'],
                twitter['description'],
                twitter['location'],
                twitter['profile_image_url']
            )
            result.append(twitter)
        # Close Neo4j session and driver
        session.close()
        return result
    except Exception as error:
        return error


# Delete Twitter account record
def delete(speaker):
    try:
        # Open database session
        session = db.session()
        # Retrieve Speaker who has Twitter account by relationship
        session.run(
            "MATCH (s:Speaker)-[r:HAS_TWITTER]->(twitter) "
            "WHERE s.firstname = $firstname "
            "AND s.middleinitial = $middleinitial "
            "AND s.lastname = $lastname "
            "DELETE r, twitter",
            firstname=speaker.firstname,
            middleinitial=speaker.middleinitial,
            lastname=speaker.lastname
        )
        session.close()
    except Exception as error:
        return error


# Search Speaker in Twitter API
def search_user_in_twitter(speaker_name):
    search_api_url = "https://api.twitter.com/1.1/users/search.json?q={}&count=10"\
        .format(speaker_name)
    response = requests.get(search_api_url, auth=twitter_api)
    return response.json()


# Get Speaker record from Twitter 'screen_name'
def get_speaker_from_twitter(screen_name):
    session = db.session()
    # Retrieve all Tweet records
    speaker = session.run(
        "MATCH (t:Twitter)<-[:HAS_TWITTER]-(speaker) "
        "WHERE t.screen_name = $screen_name "
        "RETURN speaker",
        screen_name=screen_name
    )
    return speaker.data()[0]['speaker']
