""" Category Model layer """
from app.c.main import db, get_by_http_request
from config import root_url
import app.m.talk as talkModel
import helpers


# Category Class constructor
class Category:
    """Category Class"""
    def __init__(self, name):
        self.name = name

    # Method to generate slug from Category name
    def get_slug(self):
        return self.name.replace(" ", "+").lower()

    # Method to generate url from Category name
    def get_url(self):
        return root_url + "/topics/" + self.name.replace(" ", "+").lower()


# Save Category record in the database
def add(slots):
    try:
        # Open database session
        with db.session() as session:
            # Initiate transaction
            tx = session.begin_transaction()
            # Evaluate if Category exists
            exists = tx.run(
                "MATCH (c:Category {name: $name}) RETURN c",
                name=slots['name']
            )
            if exists.single() is None:
                slots = {'name': slots['name']}
                # Save data in database
                tx.run(
                    "CREATE (c:Category $slots)",
                    slots=slots
                )
                # Commit and close transaction
                tx.commit()
                tx.close()
                # Close database session
                db.close()
                return True
            else:
                return False
    except Exception as error:
        return error


# Retrieve and return all categories as list of Category objects
def retrieve_all():
    try:
        result = []
        # Open Neo4j session and driver
        session = db.session()
        # Retrieve all Category records
        categories = session.run(
            "MATCH (c:Category) RETURN c AS category"
        )
        for c in categories:
            # Instantiate Class object
            category = Category(
                c.data()['category']['name'],
            )
            result.append(category)
        # Close Neo4j session and driver
        session.close()
        return result
    except Exception as error:
        return error


# Retrieve and returns Category object
def retrieve(category):
    try:
        # Open Neo4j session and driver
        session = db.session()
        category = session.run(
            "MATCH (c:Category {name: $name}) RETURN c AS category",
            name=category
        )
        category = category.data()[0]['category']
        category = Category(category['name'])
        # Close Neo4j session and driver
        session.close()
        # Instantiate Class object
        return category
    except Exception as error:
        return error


# Scrape HTML elements from web page
def scrape_all():
    # HTTP request data from web page
    data = get_by_http_request(root_url + "/topics")
    # Parse data from HTML elements
    categories = data.html.find('a.mb-1 > div', first=False)
    # Generate and return list of categories
    result = []
    for c in categories:
        slots = {'name': c.text}
        result.append(slots)
    return result


# Pagination for categories
def get_category_pages_number(category):
    # Build URL string
    url = talkModel.talk_url_by_page_number(category, 1)
    # HTTP request the Category page
    data = get_by_http_request(url)
    # Calculate number of pages in Category
    pagination = data.html.find('a.pagination__item', first=False)
    if not pagination:
        num_pages = 1
    else:
        num_pages = int(pagination[-1].text)
    # Create a list with all page numbers
    num_pages = num_pages + 1
    pages = list(range(1, num_pages))
    return pages


def verify_urls(category):
    # Delay scraping time randomly
    helpers.set_random_delay(1, 10)
    # HTTP request data from web page
    data = get_by_http_request(category.get_url())
    status = data.status_code
    return status
