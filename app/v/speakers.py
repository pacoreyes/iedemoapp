""" Speaker View layer """


# View for scrape all the Speakers
def scrape_all(response, speaker):
    if response is True:
        print("> Saved Speaker record: '{}'".format(speaker))
    elif response is False:
        print("x Speaker record '{}' not saved, already exists".format(speaker))
    else:
        print("Error: '{}'".format(response))
