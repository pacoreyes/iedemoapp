""" Twitter View layer """


# Colors Class
class Colors:
    HIGHLIGHT = '\033[93m'
    END = '\033[0m'
    BOLD = '\033[91m'
    GREEN = '\033[96m'


# View of add() procedure for Twitter account linked to Speaker
def add(twitter, s):
    print(f"Twitter account {Colors.BOLD}"
          + "@" + twitter['screen_name'] + f"{Colors.END}"
          + f" linked to Speaker {Colors.BOLD}"
          + s.get_full_name()
          + f"{Colors.END}.")
    print()
    print("=========================================")


# View of menu to link Speakers to Twitter accounts
# this view uses colorization to help guiding the User Interaction
def link_all_menu(speaker, talks, twitter_accounts):
    print()
    print("Choose a Twitter account to link the speaker to: "
          + f"{Colors.HIGHLIGHT}"
          + speaker.get_full_name()
          + f"{Colors.END}")
    print("• Bio: {}".format(speaker.bio))
    print("• Picture: {}.".format(speaker.photo_url))
    for t in talks:
        print("• Spoke at: " + t.title + " ({}).".format(t.url))
    print("---")
    # Show list of Twitter accounts potentially related
    for ta in range(len(twitter_accounts)):
        print("{}. ".format(ta + 1)
              + f"{Colors.BOLD}"
              + twitter_accounts[ta]['name'] + f"{Colors.END}"
              + " ({}), verified: {}: {}"
              .format('https://twitter.com/'
                      + twitter_accounts[ta]['screen_name'],
                      twitter_accounts[ta]['verified'],
                      twitter_accounts[ta]['description']))
    # Show list other options in the menu
    print("0. Skip record")
    print("99. Unlink Twitter account and Skip record")
    return int(input("---> Enter your choice: "))


# View if 'not found' Twitter account for Speaker
def not_found(speaker):
    print()
    print("Not related account found in Twitter API for the Speaker: "
          + f"{Colors.HIGHLIGHT}"
          + speaker.get_full_name()
          + f"{Colors.END}")
    input("Press any key to retrieve next Speaker...")
    print()
    print("=========================================")


# View if 'skip' Twitter account for Speaker
def skip():
    print(f"{Colors.GREEN}Record skipped{Colors.END}.")


# View if choice is not within the inspected choices
def wrong():
    print("Wrong Choice")
