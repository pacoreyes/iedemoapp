""" Tweet View layer """


# View for extract Tweets
def extract(response, tweet_id):
    # Evaluate response from Model
    if response is True:
        print("> Saved Tweet: '{}'".format(tweet_id))
    elif response is False:
        print("x Tweet '{}' already exists".format(tweet_id))
    else:
        print("Error: '{}'".format(response))
