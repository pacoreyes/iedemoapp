""" Categories View layer """


# View for the scrape procedure
def scrape_all(response, category):
    # Evaluate response from Model
    if response is True:
        print("> Saved Category: '{}'".format(category['name']))
    elif response is False:
        print("x Category '{}' already exists".format(category['name']))
    else:
        print("Error: '{}'".format(response))


# View for the URL verification procedure
def verify_urls(status, category):
    # If URL is dead
    if status == 200:
        print("> HTTP Status 200 OK --> '{}'".format(category.name))
    elif status == 404:  # If URL is alive
        print("x HTTP Status 404 ~ Broken URL: '{}'".format(category.get_url()))
    else:
        print("Network Error")
