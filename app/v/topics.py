""" Topic View layer """


# View for extract Topic
def extract(response, topic, speaker):
    # Evaluate response from Model
    if response is True:
        print("> Extracted Topic '{}' from {}'s content (Relevance confidence: {})"
              .format(topic['name'], speaker, topic['confidence']))
    else:
        print("Error: '{}'".format(response))
