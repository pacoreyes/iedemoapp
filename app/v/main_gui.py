""" Main Graphic User Interface """
import app.m.category as categoryModel
import app.c.categories as Categories
import app.m.talk as talkModel
import app.c.talks as Talks
import app.m.speaker as speakerModel
import app.c.speakers as Speakers
import app.m.twitter as twitterModel
import app.c.twitters as Twitters
import app.m.tweet as tweetModel
import app.c.tweets as Tweets
import app.m.topic as topicModel
import app.c.topics as Topic
import utilities as Utilities
from tkinter import *
import tkinter as tk


class MenuBar(Frame):
    """Menu Class"""
    def __init__(self, master=None):
        Frame.__init__(self, master)
        self.master = master
        # Create object
        menubar = Menu(self.master)
        self.master.config(menu=menubar)

        # Add submenu File
        file = Menu(menubar, tearoff=0)
        menubar.add_cascade(label='File', menu=file)
        file.add_command(label='Summary', command=home_ui)
        file.add_separator()
        file.add_command(label='Exit', command=root.destroy)

        # Add Scraper Menu option
        scraper = Menu(menubar, tearoff=0)
        menubar.add_cascade(label='Scrape', menu=scraper)
        categories = Menu(menubar, tearoff=0)
        talks = Menu(menubar, tearoff=0)
        speakers = Menu(menubar, tearoff=0)

        # Add submenu Scraper > Categories
        scraper.add_cascade(label='Categories...', menu=categories)
        categories.add_command(label='Scrape All Categories',
                               command=scrape_all_categories_gui)
        categories.add_command(label="Verify URLs' Scraped Categories",
                               command=verify_category_urls_gui)
        # Add submenu Scraper > Talks
        scraper.add_cascade(label='Talks...', menu=talks)
        talks.add_command(label='Scrape All Talks',
                          command=scrape_all_talks_gui)
        talks.add_command(label='Scrape 1 talk',
                          command=scrape_talk_gui)
        # Add submenu Scraper > Speakers
        scraper.add_cascade(label='Speakers...', menu=speakers)
        speakers.add_command(label='Scrape All Speakers',
                             command=scrape_all_speakers_gui)

        # Add Twitter Menu option
        twitter = Menu(menubar, tearoff=0)
        menubar.add_cascade(label='Twitter', menu=twitter)
        twitter.add_command(label="Link/Unlink Speakers to Twitter accounts",
                            command=link_all_speakers_to_twitter_gui)
        twitter.add_command(label='Extract Tweets from All Speakers',
                            command=extract_tweets_from_all_speakers_gui)

        # Add Topic Menu option
        topic = Menu(menubar, tearoff=0)
        menubar.add_cascade(label='Topics', menu=topic)
        topic.add_command(label="Extract Topics",
                          command=extract_topics_from_tweets_gui)

        # Add Utilities Menu option
        utilities = Menu(menubar, tearoff=0)
        menubar.add_cascade(label='Utilities', menu=utilities)
        utilities.add_command(label='Extract robots.txt', command=scrape_robots_txt_gui)
        utilities.add_command(label='Clear Data', command=clear_data_gui)

        # Add Help Menu option
        help_ = Menu(menubar, tearoff=0)
        menubar.add_cascade(label='Help', menu=help_)
        help_.add_command(label='Help', command=None)
        help_.add_separator()
        help_.add_command(label='About IEApp', command=home_ui)


def home_ui():
    hide_all_frames()
    home_frame.pack(fill="both", expand=1)
    title = Label(home_frame, text="Information Extraction Demo App (2020)",
                  font='Helvetica 18 bold')
    title.pack(side=TOP, padx=20, pady=20)
    description_text = \
        'IEDApp is an educational application developed for the seminar \"Information Extraction\n ' \
        'from Web Resources\", given by Juan-Francisco Reyes (reyes@b-tu.edu) at the Department \n ' \
        'of Internet Technology in Brandenburg University of Technology, Cottbus, Germany.\n' \
        '\n This application presents different methods to extract information from the open web and APIs.\n'
    label = Label(home_frame, text=description_text)
    label.pack(side=TOP, anchor=NW, padx=20, pady=0)

    # Retrieve and show total of Categories
    categories = categoryModel.retrieve_all()
    c_label = Label(home_frame,
                    text="Total Categories scraped: " + str(len(categories)),
                    font='Helvetica 14 bold')
    c_label.pack(side=TOP, anchor=NW, padx=20, pady=0)
    # Retrieve and show total of  Talks
    talks = talkModel.retrieve_all()
    t_label = Label(home_frame,
                    text="Total Talks scraped: " + str(len(talks)),
                    font='Helvetica 14 bold')
    t_label.pack(side=TOP, anchor=NW, padx=20, pady=0)
    # Retrieve and show total of Speakers
    speakers = speakerModel.retrieve_all()
    s_label = Label(home_frame,
                    text="Total Speakers scraped: " + str(len(speakers)),
                    font='Helvetica 14 bold')
    s_label.pack(side=TOP, anchor=NW, padx=20, pady=0)
    # Retrieve and show total of  Twitter accounts
    twitters = twitterModel.retrieve_all()
    t_label = Label(home_frame,
                    text="Total Twitter accounts linked: " + str(len(twitters)),
                    font='Helvetica 14 bold')
    t_label.pack(side=TOP, anchor=NW, padx=20, pady=0)
    # Retrieve and show total of Tweets
    tweets = tweetModel.retrieve_all()
    s_label = Label(home_frame,
                    text="Total Tweets extracted: " + str(len(tweets)),
                    font='Helvetica 14 bold')
    s_label.pack(side=TOP, anchor=NW, padx=20, pady=0)
    # Retrieve and show total of Topics
    topics = topicModel.retrieve_all()
    s_label = Label(home_frame,
                    text="Total Topics extracted: " + str(len(topics)),
                    font='Helvetica 14 bold')
    s_label.pack(side=TOP, anchor=NW, padx=20, pady=0)


# Categories menu functions
def scrape_all_categories_gui():
    hide_all_frames()
    scrape_categories_frame.pack(fill="both", expand=1)
    title = Label(scrape_categories_frame, text="Scrape Categories",
                  font='Helvetica 18 bold')
    title.pack(side=TOP, anchor=NW, padx=20, pady=20)
    label = Label(scrape_categories_frame,
                  text="All Categories on TED website will be scraped:")
    label.pack(side=TOP, anchor=NW, padx=20, pady=20)
    button = Button(scrape_categories_frame,
                    text=" Start Scrape ",
                    command=Categories.scrape_all)
    button.pack(side=TOP, anchor=NW, padx=20, pady=0)


def verify_category_urls_gui():
    hide_all_frames()
    verify_category_urls_frame.pack(fill="both", expand=1)
    title = Label(verify_category_urls_frame, text="Verify Categories' URLs",
                  font='Helvetica 18 bold')
    title.pack(side=TOP, anchor=NW, padx=20, pady=20)
    label = Label(verify_category_urls_frame,
                  text="All URLs of individual Categories will be verified:")
    label.pack(side=TOP, anchor=NW, padx=20, pady=20)
    button = Button(verify_category_urls_frame,
                    text=" Start Verification ",
                    command=Categories.verify_urls)
    button.pack(side=TOP, anchor=NW, padx=20, pady=0)


# Categories menu functions
def scrape_all_talks_gui():
    hide_all_frames()
    scrape_talks_frame.pack(fill="both", expand=1)
    title = Label(scrape_talks_frame, text="Scrape Talks",
                  font='Helvetica 18 bold')
    title.pack(side=TOP, anchor=NW, padx=20, pady=20)
    label = tk.Label(scrape_talks_frame,
                     text="Select a Category to scrape all its Talks:")
    label.pack(side=TOP, anchor=NW, padx=20, pady=20)

    # Load names from Category table
    category_recs = [c.name for c in categoryModel.retrieve_all()]
    category_recs = sorted(category_recs, key=None, reverse=False)

    def capture_key(entered_key):
        key = entered_key.widget.get()
        key = key.strip().lower()
        if key == '':
            data = category_recs
        else:
            data = [c for c in category_recs if key in c.lower()]
        update_combo(data)

    def update_combo(options):
        listbox.delete(0, 'end')
        options = sorted(options, key=str.lower)
        for o in options:
            listbox.insert('end', o)

    def on_select(event):
        category = event.widget.get(event.widget.curselection())
        entry.delete(0, END)
        entry.insert(0, category)
        return category

    def call_scrape():
        category = entry.get()
        Talks.scrape_all(category)

    # Display Search box
    entry = tk.Entry(scrape_talks_frame)
    entry.pack(side=TOP, anchor=NW, padx=20, pady=0)
    entry.focus_set()
    entry.bind('<KeyRelease>', capture_key)
    # Display Combobox
    listbox = tk.Listbox(scrape_talks_frame)
    listbox.pack(side=TOP, anchor=NW, padx=20, pady=0)
    listbox.bind('<<ListboxSelect>>', on_select)
    update_combo(category_recs)
    # Display button
    button = tk.Button(scrape_talks_frame,
                       text=" Start Scrape ",
                       command=call_scrape)
    button.pack(side=TOP, anchor=NW, padx=20, pady=20)


def scrape_talk_gui():
    def call_scrape():
        url = entry.get()
        Talks.scrape(url)

    hide_all_frames()
    scrape_1_talk_frame.pack(fill="both", expand=1)
    title = Label(scrape_1_talk_frame, text="Scrape Talk",
                  font='Helvetica 18 bold')
    title.pack(side=TOP, anchor=NW, padx=20, pady=20)
    label = Label(scrape_1_talk_frame,
                  text="Enter an individual TED Talk's URL to be scraped:")
    label.pack(side=TOP, anchor=NW, padx=20, pady=20)
    entry = tk.Entry(scrape_1_talk_frame, width=40)
    entry.pack(side=TOP, anchor=NW, padx=20, pady=0)
    entry.focus_set()
    button = Button(scrape_1_talk_frame,
                    text=" Scrape Talk ",
                    command=call_scrape)
    button.pack(side=TOP, anchor=NW, padx=20, pady=20)


# Speakers menu functions
def scrape_all_speakers_gui():
    hide_all_frames()
    scrape_speakers_frame.pack(fill="both", expand=1)
    title = Label(scrape_speakers_frame, text="Scrape Speakers",
                  font='Helvetica 18 bold')
    title.pack(side=TOP, anchor=NW, padx=20, pady=20)
    label = Label(scrape_speakers_frame,
                  text="The Speakers from all scraped "
                       "TED Talks will be scraped:")
    label.pack(side=TOP, anchor=NW, padx=20, pady=20)
    button = Button(scrape_speakers_frame,
                    text=" Start Scrape ",
                    command=Speakers.scrape_all)
    button.pack(side=TOP, anchor=NW, padx=20, pady=0)


# Extract robots.txt
def scrape_robots_txt_gui():
    hide_all_frames()
    scrape_robot_frame.pack(fill="both", expand=1)
    title = Label(scrape_robot_frame, text="Scrape 'robots.txt'",
                  font='Helvetica 18 bold')
    title.pack(side=TOP, anchor=NW, padx=20, pady=20)
    label = Label(scrape_robot_frame,
                  text="The 'robots.txt' of www.ted.com will be extracted")
    label.pack(side=TOP, anchor=NW, padx=20, pady=20)
    button = Button(scrape_robot_frame,
                    text=" Scrape robots.txt ",
                    command=Utilities.extract_robots_txt)
    button.pack(side=TOP, anchor=NW, padx=20, pady=0)


# Link all Speakers to Twitter
def link_all_speakers_to_twitter_gui():
    hide_all_frames()
    link_speakers_to_twitter_frame.pack(fill="both", expand=1)
    title = Label(link_speakers_to_twitter_frame, text="Link Speakers to Twitter accounts",
                  font='Helvetica 18 bold')
    title.pack(side=TOP, anchor=NW, padx=20, pady=20)
    label = Label(link_speakers_to_twitter_frame,
                  text="All Speakers will be linked to suggested Twitter accounts:")
    label.pack(side=TOP, anchor=NW, padx=20, pady=20)
    button = Button(link_speakers_to_twitter_frame,
                    text=" Start Linking ",
                    command=Twitters.link_all)
    button.pack(side=TOP, anchor=NW, padx=20, pady=0)


# Extract Tweets from all Speakers
def extract_tweets_from_all_speakers_gui():
    hide_all_frames()
    extract_tweets_from_speakers_frame.pack(fill="both", expand=1)
    title = Label(extract_tweets_from_speakers_frame, text="Extract Tweets from Twitter accounts",
                  font='Helvetica 18 bold')
    title.pack(side=TOP, anchor=NW, padx=20, pady=20)
    label = Label(extract_tweets_from_speakers_frame,
                  text="Latest Tweets will be extracted from Twitter accounts:")
    label.pack(side=TOP, anchor=NW, padx=20, pady=20)
    button = Button(extract_tweets_from_speakers_frame,
                    text=" Start Extraction ",
                    command=Tweets.extract)
    button.pack(side=TOP, anchor=NW, padx=20, pady=0)


# Extract Topic from all Tweets, Speaker Bio and Talk
def extract_topics_from_tweets_gui():
    hide_all_frames()
    extract_topics_from_tweets_frame.pack(fill="both", expand=1)
    title = Label(extract_topics_from_tweets_frame,
                  text="Extract Topics from Tweets, Speaker Bio and Talk",
                  font='Helvetica 18 bold')
    title.pack(side=TOP, anchor=NW, padx=20, pady=20)
    label = Label(extract_topics_from_tweets_frame,
                  text="Topics will be extracted from the content of the Tweets, Speaker Bio and Talk:")
    label.pack(side=TOP, anchor=NW, padx=20, pady=20)
    button = Button(extract_topics_from_tweets_frame,
                    text=" Start Extraction ",
                    command=Topic.extract)
    button.pack(side=TOP, anchor=NW, padx=20, pady=0)


# Extract robots.txt
def clear_data_gui():
    hide_all_frames()
    clear_data_frame.pack(fill="both", expand=1)
    title = Label(clear_data_frame, text="Clear Data",
                  font='Helvetica 18 bold')
    title.pack(side=TOP, anchor=NW, padx=20, pady=20)

    button1 = Button(clear_data_frame,
                     text=" Clear Categories ",
                     command=Utilities.clear_categories)
    button1.pack(side=TOP, anchor=NW, padx=20, pady=0)

    button2 = Button(clear_data_frame,
                     text=" Clear Talks ",
                     command=Utilities.clear_talks)
    button2.pack(side=TOP, anchor=NW, padx=20, pady=0)

    button3 = Button(clear_data_frame,
                     text=" Clear Speakers",
                     command=Utilities.clear_speakers)
    button3.pack(side=TOP, anchor=NW, padx=20, pady=0)

    button4 = Button(clear_data_frame,
                     text=" Clear Twitter accounts ",
                     command=Utilities.clear_twitters)
    button4.pack(side=TOP, anchor=NW, padx=20, pady=0)

    button5 = Button(clear_data_frame,
                     text=" Clear Tweets",
                     command=Utilities.clear_tweets)
    button5.pack(side=TOP, anchor=NW, padx=20, pady=0)

    button6 = Button(clear_data_frame,
                     text=" Clear Topics",
                     command=Utilities.clear_topics)
    button6.pack(side=TOP, anchor=NW, padx=20, pady=0)

    button7 = Button(clear_data_frame,
                     text=" Clear All Data",
                     command=Utilities.clear_all_data)
    button7.pack(side=TOP, anchor=NW, padx=20, pady=0)


def hide_all_frames():
    frames = [home_frame,
              scrape_categories_frame,
              verify_category_urls_frame,
              scrape_talks_frame,
              scrape_1_talk_frame,
              scrape_speakers_frame,
              scrape_robot_frame,
              link_speakers_to_twitter_frame,
              extract_tweets_from_speakers_frame,
              extract_topics_from_tweets_frame,
              clear_data_frame]
    for frame in frames:
        for ch in frame.winfo_children():
            ch.destroy()
        frame.pack_forget()


# Create the UI Window -- the main component of the app UI
root = Tk()
# Create the menubar
app = MenuBar(root)
# Set main UI components
root.wm_title("Information Extraction Demo App - TED Talks")
root.geometry('700x500+0+0')
root.config(background="#FFFFFF")
# Create frames
home_frame = Frame(root, width=700, height=500)
scrape_categories_frame = Frame(root, width=700, height=500)
verify_category_urls_frame = Frame(root, width=700, height=500)
scrape_talks_frame = Frame(root, width=700, height=500)
scrape_1_talk_frame = Frame(root, width=700, height=500)
scrape_speakers_frame = Frame(root, width=700, height=500)
scrape_robot_frame = Frame(root, width=700, height=500)
link_speakers_to_twitter_frame = Frame(root, width=700, height=500)
extract_tweets_from_speakers_frame = Frame(root, width=700, height=500)
extract_topics_from_tweets_frame = Frame(root, width=700, height=500)
clear_data_frame = Frame(root, width=700, height=500)
