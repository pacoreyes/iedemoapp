""" Procedures to maintain the database """
from app.c.main import db, get_by_http_request
from config import root_url
import helpers


# Extract robots.txt file from website
def extract_robots_txt():
    data = get_by_http_request(root_url + "/robots.txt")
    try:
        # Save data .txt file
        helpers.save_file(data.text, "data/", "robots", "txt")
        print("\n> File 'robots.txt' extracted and stored in 'data' directory\n")
    except OSError as error:
        print("OS error: {0}".format(error))


# Clear Category records
def clear_categories():
    try:
        s = db.session()
        # Retrieve all Category records
        s.run(
            "MATCH (n:Category) DETACH DELETE n"
        )
        s.close()
        print("\nAll Category records deleted\n")
    except Exception as error:
        return error


# Clear Talk records
def clear_talks():
    try:
        s = db.session()
        # Retrieve all Talk records
        s.run(
            "MATCH (n:Talk) DETACH DELETE n"
        )
        s.close()
        print("\nAll Talk records deleted\n")
    except Exception as error:
        return error


# Clear Speaker records
def clear_speakers():
    try:
        s = db.session()
        # Retrieve all Speaker records
        s.run(
            "MATCH (n:Speaker) DETACH DELETE n"
        )
        s.close()
        print("\nAll Speaker records deleted\n")
    except Exception as error:
        return error


# Clear Twitter records
def clear_twitters():
    try:
        s = db.session()
        # Retrieve all Twitter records
        s.run(
            "MATCH (n:Twitter) DETACH DELETE n"
        )
        s.close()
        print("\nAll Twitter records deleted\n")
    except Exception as error:
        return error


# Clear Tweet records
def clear_tweets():
    try:
        s = db.session()
        # Retrieve all Tweet records
        s.run(
            "MATCH (n:Tweet) DETACH DELETE n"
        )
        s.close()
        print("\nAll Tweet records deleted\n")
    except Exception as error:
        return error


# Clear Topic records
def clear_topics():
    try:
        s = db.session()
        # Retrieve all Topic records
        s.run(
            "MATCH (n:Topic) DETACH DELETE n"
        )
        s.close()
        print("\nAll Topic records deleted\n")
    except Exception as error:
        return error


# Clear all the records
def clear_all_data():
    try:
        s = db.session()
        # Retrieve all Speaker records
        s.run(
            "MATCH (n:Category) DETACH DELETE n"
        )
        print("\n> Category records deleted\n")
        s.run(
            "MATCH (n:Talk) DETACH DELETE n"
        )
        print("\n> Talk records deleted\n")
        s.run(
            "MATCH (n:Speaker) DETACH DELETE n"
        )
        print("\n> Speaker records deleted\n")
        s.close()
        s.run(
            "MATCH (n:Twitter) DETACH DELETE n"
        )
        print("\n> Twitter records deleted\n")
        s.run(
            "MATCH (n:Tweet) DETACH DELETE n"
        )
        print("\n> Tweet records deleted\n")
        s.run(
            "MATCH (n:Topic) DETACH DELETE n"
        )
        print("\n> Topic records deleted\n")
        s.close()
        print("\nAll records deleted\n")
    except Exception as error:
        return error
