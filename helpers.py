""" Common helper functions """
from config import base_dir
import random
import time
import json
import os.path
import re


# Set random delay time between min and max time in seconds
def set_random_delay(min_delay, max_delay):
    seconds = random.randint(min_delay, max_delay)
    time.sleep(seconds)


# Sanitize data embedded in HTML in JSON format
def sanitize_talk_data(t):
    # Remove undesirable content -- pre and post JSON
    t = t.replace('q("talkPage.init",{"el":"[data-talk-page]",'
                  '"__INITIAL_DATA__":', '')
    t = t[:-2]
    # Count occurrences of substring 'whylisten' in string 't'
    occurrences = t.count("whylisten")
    i = 1
    # Define position of pointer in the string 't'
    pointer = 0
    wrong_substrings = []
    # Separate substring 'whylisten' from string 't'
    while i <= occurrences:
        start = t.find('"whylisten"', pointer)
        end = t.find('}', start)
        pointer = end
        wrong_substring = t[start:end]
        wrong_substrings.append(wrong_substring)
        i += 1
    # Replace iteratively substring 'whylisten' with cleaned version
    for w in wrong_substrings:
        t = t.replace(w, '"whylisten":""')
    # Convert to JSON object
    t = json.loads(t)
    # Save Talk in JSON format
    save_file(t, "data/", "ted_talk", "json")
    return t


def save_file(data, path, file_name, extension):
    try:
        if extension == "json":
            file_path = os.path.join(
                base_dir,
                path + file_name + ".json"
            )
            with open(file_path, 'w') as file:
                json.dump(data, file, sort_keys=True, indent=2)
                file.close()
        else:
            file_path = os.path.join(
                base_dir,
                path + file_name + "." + extension
            )
            file = open(file_path, "w")
            file.write(data)
            file.close()
    except OSError as error:
        print("OS error: {0}".format(error))


def open_file(path, name, extension):
    try:
        if extension == "json":
            file_path = os.path.join(
                base_dir,
                path + name + ".json"
            )
            with open(file_path) as file:
                data = json.load(file)
                file.close()
        else:
            file_path = os.path.join(
                base_dir,
                path + name + "." + extension
            )
            with open(file_path, "r") as file:
                data = file.read()
                file.close()
        return data
    except OSError as error:
        print("OS error: {0}".format(error))


# Sanitize Tweet data and concatenate  it in one string
def sanitize_tweet_data(tweets):
    text = ""
    for tw in tweets:
        # Concatenate all Tweets in one 'Text' string
        text = text + tw.text
    # Remove urls from text
    text = re.sub(r'http\S+', '', text)
    # Remove split lines
    text = os.linesep.join([s for s in text.splitlines() if s])
    # Remove line breakers
    text = text.replace('\n', ' ')
    # Remove Twitter names (@user)
    text = re.sub('@[^\s]+', '', text)
    # Remove # from hashtags
    text = text.replace('#', ' ')
    # Remove emojis
    text = de_emojify(text)
    return text


# Remove emojis from string
# Took it from: https://stackoverflow.com/questions/33404752/removing-emojis-from-a-string-in-python
def de_emojify(text):
    regrex_pattern = \
        re.compile(pattern="["
                           u"\U0001F600-\U0001F64F"  # emoticons
                           u"\U0001F300-\U0001F5FF"  # symbols & pictographs
                           u"\U0001F680-\U0001F6FF"  # transport & map symbols
                           u"\U0001F1E0-\U0001F1FF"  # flags (iOS)
                           "]+", flags=re.UNICODE)
    return regrex_pattern.sub(r'', text)
