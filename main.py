"""Procedure to load Tkinter UI"""
from app.v.main_gui import root, home_ui


if __name__ == "__main__":
    # Load Home User Interface
    home_ui()
    # Monitor and update the UI
    root.mainloop()
